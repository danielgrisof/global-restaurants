# Global Restaurants


An app built on Swift language with cocoapods to make the process faster, 
on the project you will find the Alamofire, AlamofireObjectMapper and SDWebImage frameworks.

I used this technology because it is faster to implement, easy for outsider programers understanding, if the app grow or need to fix something inside is easy to modify. The VIPER archicteture give you more layers to mantain your app more independent, something like, if you have some problem with the API request or response, user interaction, logic of the app, you have one file for each. 


Mockup: https://invis.io/GMLOPC87TSW
