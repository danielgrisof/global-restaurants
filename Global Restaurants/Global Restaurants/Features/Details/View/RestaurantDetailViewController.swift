//
//  RestaurantDetailViewController.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit
import SDWebImage

class RestaurantDetailViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var imgRestauarant: UIImageView!
    @IBOutlet weak var lblRestaurantAddress: UILabel!
    @IBOutlet weak var lblRestaurantLocality: UILabel!
    @IBOutlet weak var lblRestaurantCity: UILabel!
    @IBOutlet weak var lblRestaurantRate: UILabel!
    @IBOutlet weak var btnRestaurantURL: UIButton!
    
    //MARK: Variables
	var presenter: RestaurantDetailPresenterProtocol?
    var infoRestaurant = [Restaurants]()
    var urlRestaurant = ""

    //MARK: - App life Cicle
	override func viewDidLoad() {
        super.viewDidLoad()
        self.showRestaurantDetails()
    }
    
    //MARK: - Functions
    private func showRestaurantDetails() {
        self.infoRestaurant.forEach { (restaurant) in
            self.title = restaurant.restaurantName
            self.lblRestaurantAddress.text = "Address: \(restaurant.restaurantAddress ?? "Address")"
            self.lblRestaurantLocality.text = "Locality: \(restaurant.restaurantLocality ?? "Locality")"
            self.lblRestaurantCity.text = "City: \(restaurant.restaurantCity ?? "City")"
            self.lblRestaurantRate.text = "Aggregate Rating: \(restaurant.restaurantRating ?? "0.0")" 
            
            if let url = restaurant.restuarantURL {
                self.btnRestaurantURL.isHidden = false
                self.urlRestaurant = url
            }else {
                self.btnRestaurantURL.isHidden = true
            }
            
            if restaurant.restaurantFeaturedImage == "" {
                self.imgRestauarant.sd_setImage(with: URL(string: restaurant.restaurantThumb!), placeholderImage: UIImage(named: "placeHolderRestaurant"), options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: nil)
            }
            
            self.imgRestauarant.sd_setImage(with: URL(string: restaurant.restaurantFeaturedImage!), placeholderImage: UIImage(named: "placeHolderRestaurant"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
        }
    }
    
    @IBAction func openWebSite() {
        UIApplication.shared.open(URL(string: urlRestaurant)!, options: [:], completionHandler: nil)
    }
    
}

//MARK: - Extension to implement RestaurantDetailViewControllerProtocol
extension RestaurantDetailViewController: RestaurantDetailViewControllerProtocol {
    
}
