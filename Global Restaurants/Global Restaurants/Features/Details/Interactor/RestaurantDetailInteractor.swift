//
//  RestaurantDetailInteractor.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation

class RestaurantDetailInteractor {

    //MARK: Variables
    var presenter: RestaurantDetailPresenterProtocol?
}

//MARK: - Extension to implement RestaurantDetailInteractorProtocol
extension RestaurantDetailInteractor: RestaurantDetailInteractorProtocol {
    
}
