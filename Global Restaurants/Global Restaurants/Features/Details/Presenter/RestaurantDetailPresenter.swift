//
//  RestaurantDetailPresenter.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class RestaurantDetailPresenter {

    //MARK: Variables
    var view: RestaurantDetailViewControllerProtocol?
    var interactor: RestaurantDetailInteractorProtocol?
    var router: RestaurantDetailRouterProtocol?

    init(view: RestaurantDetailViewControllerProtocol, interactor: RestaurantDetailInteractorProtocol, router: RestaurantDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

//MARK: - Extension to implement RestaurantDetailPresenterProtocol
extension RestaurantDetailPresenter: RestaurantDetailPresenterProtocol {
    
}
