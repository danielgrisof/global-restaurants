//
//  RestaurantDetailRouter.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import UIKit

class RestaurantDetailRouter: BaseRouter {
    
    static func registerWithNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: createModule())
    }
    
    static func createModule() -> RestaurantDetailViewController {
        // Change to get view from storyboard if not using progammatic UI
        let viewController: RestaurantDetailViewController = RestaurantDetailViewController()
        let router: RestaurantDetailRouter = RestaurantDetailRouter(viewController: viewController)
        let interactor: RestaurantDetailInteractor = RestaurantDetailInteractor()
        let presenter: RestaurantDetailPresenter = RestaurantDetailPresenter(view: viewController, interactor: interactor, router: router)
        
        viewController.presenter = presenter
        interactor.presenter = presenter
        router.viewController = viewController
        
        return viewController
    }
}

//MARK: - Extension to implement RestaurantDetailRouterProtocol
extension RestaurantDetailRouter: RestaurantDetailRouterProtocol {
    
}
