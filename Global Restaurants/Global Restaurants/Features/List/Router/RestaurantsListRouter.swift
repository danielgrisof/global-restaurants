//
//  RestaurantsListRouter.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import UIKit

class RestaurantsListRouter: BaseRouter {
    
    //MARK: - Functions
    static func registerWithNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: createModule())
    }
    
    static func createModule() -> RestaurantsListViewController {
        // Change to get view from storyboard if not using progammatic UI
        let viewController: RestaurantsListViewController = RestaurantsListViewController()
        let router: RestaurantsListRouter = RestaurantsListRouter(viewController: viewController)
        let interactor: RestaurantsListInteractor = RestaurantsListInteractor()
        let presenter: RestaurantsListPresenter = RestaurantsListPresenter(view: viewController, interactor: interactor, router: router)
        
        viewController.presenter = presenter
        interactor.presenter = presenter
        router.viewController = viewController
        
        return viewController
    }
    
    private func showDetails(restaurants: [Restaurants]) {
        let vc = RestaurantDetailRouter.createModule()
        vc.infoRestaurant = restaurants
        self.pushToView(viewController: vc, animated: true)
    }
}

//MARK: - Extension to implement RestaurantsListRouterProtocol
extension RestaurantsListRouter: RestaurantsListRouterProtocol {
    
    func goToDetail(restaurants: [Restaurants]) {
        self.showDetails(restaurants: restaurants)
    }
}
