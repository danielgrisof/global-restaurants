//
//  RestaurantsListInteractor.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation
import Alamofire
import ObjectMapper

class RestaurantsListInteractor {

    //MARK: Variables
    var presenter: RestaurantsListPresenterProtocol?
    let generic = GenericRequest()
    
    //MARK: - Functions
    private func apiRequestRestaurant() {
        
        let headers: HTTPHeaders = [
            "user-key": "bd9b735097857e85bfde47ef51ebb239",
            "Content-Type": "application/json"
        ]
        
        generic.apiRequest(method: HTTPMethod.get, forService: search, headers: headers, completion: { (restaurantList: RestaurantsListEntity) in
            if restaurantList.restaurants?.count != 0 {
                restaurantList.restaurants?.forEach({ (restaurant) in
                    self.presenter?.resultRequest(restaurants: [restaurant])
                })
            }
        }) { (error) in
            self.presenter?.errorRequest()
        }
        
    }
    
}

//MARK: - Extension to implement RestaurantsListInteractorProtocol
extension RestaurantsListInteractor: RestaurantsListInteractorProtocol {
    
    //MARK: Functions
    func getRestaurant() {
        self.apiRequestRestaurant()
    }
}
