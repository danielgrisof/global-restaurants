//
//  RestaurantsListEntity.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation
import ObjectMapper

class RestaurantsListEntity: Mappable {
    
    //MARK: Variables
    var restaurants: [Restaurants]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.restaurants <- map["restaurants"]
    }
}

class Restaurants: Mappable {
    
    //MARK: Variables
    var restaurantName: String?
    var restaurantID: Double?
    var restuarantURL: String?
    var restaurantThumb: String?
    var restaurantAddress: String?
    var restaurantLocality: String?
    var restaurantCity: String?
    var restaurantRating: String?
    var restaurantCuisines: String?
    var restaurantFeaturedImage: String?
    
    required init?(map: Map) {
        
    }
    
    //MARK: - Functions
    func mapping(map: Map) {
        self.restaurantID <- map["restaurant.id"]
        self.restaurantName <- map["restaurant.name"]
        self.restuarantURL <- map["restaurant.url"]
        self.restaurantThumb <- map["restaurant.thumb"]
        self.restaurantAddress <- map["restaurant.location.address"]
        self.restaurantLocality <- map["restaurant.location.locality"]
        self.restaurantCity <- map["restaurant.location.city"]
        self.restaurantRating <- map["restaurant.user_rating.aggregate_rating"]
        self.restaurantCuisines <- map["restaurant.cuisines"]
        self.restaurantFeaturedImage <- map["restaurant.featured_image"]
    }
}
