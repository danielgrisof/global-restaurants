//
//  RestaurantsListViewController.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class RestaurantsListViewController: UIViewController {

    //MARK: Variables
	var presenter: RestaurantsListPresenterProtocol?
    let cellIdentifier = "restaurantList"
    var restaurantArray = [Restaurants]()

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - App life Cicle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.requestRestaurants()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.setupNavigationBar()
        self.activityIndicator.startAnimating()
    }
    
    //MARK: - Functions
    private func setupNavigationBar() {
        self.title = "Restaurants"
        let backButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
    }
    
    private func registerCell() {
        self.tableView.register(UINib(nibName: "RestaurantsListCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
}

//MARK: - Extension to implement RestaurantsListViewControllerProtocol
extension RestaurantsListViewController: RestaurantsListViewControllerProtocol {
    func result(restaurants: [Restaurants]) {
        restaurants.forEach { (restaurant) in
            self.restaurantArray.append(restaurant)
        }
        self.activityIndicator.stopAnimating()
        
        self.tableView.reloadData()
    }
    
    func showAllert(title: String, message: String) {
        self.alert(title: title, message: message)
    }
}
//MARK: - Extension to implement UITableViewDelegate, UITableViewDataSource
extension RestaurantsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurantArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? RestaurantsListCell {
            cell.configureCell(restaurantInfo: [restaurantArray[indexPath.row]])
            return cell
        }else {
            return RestaurantsListCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.presenter?.detailRestaurant(restaurants: [self.restaurantArray[indexPath.row]])
    }
    
}
