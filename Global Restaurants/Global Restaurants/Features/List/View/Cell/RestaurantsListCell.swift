//
//  RestaurantsListCell.swift
//  Global Restaurants
//
//  Created by Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit
import SDWebImage

class RestaurantsListCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblNameRestaurant: UILabel!
    @IBOutlet weak var lblRateRestaurant: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(restaurantInfo info: [Restaurants]) {
        info.forEach { (restaurant) in
            self.lblNameRestaurant.text = restaurant.restaurantName ?? "Name"
            self.lblRateRestaurant.text = "Aggregate Rating: \(restaurant.restaurantRating ?? "0.0")"
            self.imgThumb.sd_setImage(with: URL(string: restaurant.restaurantThumb!), placeholderImage: UIImage(named: "placeHolderRestaurant"), options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: nil)
            
        }
    }
    
}
