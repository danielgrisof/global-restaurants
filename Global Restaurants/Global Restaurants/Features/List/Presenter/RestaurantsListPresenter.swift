//
//  RestaurantsListPresenter.swift
//  Global Restaurants
//
//  Created Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class RestaurantsListPresenter {

    //MARK: Variables
    var view: RestaurantsListViewControllerProtocol?
    var interactor: RestaurantsListInteractorProtocol?
    var router: RestaurantsListRouterProtocol?

    init(view: RestaurantsListViewControllerProtocol, interactor: RestaurantsListInteractorProtocol, router: RestaurantsListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    //MARK: - Functions
    private func request() {
        self.interactor?.getRestaurant()
    }
    
    private func result(restaurants: [Restaurants]) {
        if restaurants.count > 0 {
            restaurants.forEach { (restaurant) in
                self.view?.result(restaurants: [restaurant])
            }
        }
    }
    
    private func detailsRestaurant(restaurants: [Restaurants]) {
        self.router?.goToDetail(restaurants: restaurants)
    }
}

//MARK: - Extension to implement RestaurantsListPresenterProtocol
extension RestaurantsListPresenter: RestaurantsListPresenterProtocol {
   
    //MARK: - Functions
    func resultRequest(restaurants: [Restaurants]) {
        self.result(restaurants: restaurants)
    }
    
    func requestRestaurants() {
        self.request()
    }
    
    func detailRestaurant(restaurants: [Restaurants]) {
        self.detailsRestaurant(restaurants: restaurants)
    }
    
    func errorRequest() {
        self.view?.showAllert(title: "Error", message: "Somrthing Wrong with the service, please try again")
    }
}
