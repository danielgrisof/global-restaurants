//
//  GenericRequest.swift
//  Global Restaurants
//
//  Created by Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

struct GenericRequest {
    
    func apiRequest<T: Mappable>(method: HTTPMethod, forService: String, headers: HTTPHeaders, completion: @escaping (T) -> (), error: @escaping (NSError) -> ()) {
        
        let url = URL(string: "\(baseURL)\(forService)")!
        
        Alamofire.request(url, method: method, headers: headers).responseJSON { (response: DataResponse<Any>) in
            if response.error != nil {
                let requestError = ApiResponse.StatusCode.DomainError.Unknown
                error(NSError(domain: requestError.0, code: requestError.1, userInfo: ["message": requestError.2]))
                return
            }
            
            if response.response?.statusCode == ApiResponse.StatusCode.Successful.OK {
                let result = response.result.value
                if let jsonResult = result as? [String: AnyObject] {
                    let jsonObject = Mapper<T>().map(JSON: jsonResult)
                    completion(jsonObject!)
                }
            }
        }
    }
    
}
